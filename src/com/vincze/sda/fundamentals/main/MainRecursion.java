package com.vincze.sda.fundamentals.main;

public class MainRecursion {

		static int nrOfOperations = 0;
	public static void main(String[] args) {
//		printNumbers();
//		fibo();
//		printNumbersRecursively(0);
		int finalFibonacci = fiboRecursively(50);
//		System.out.println(finalFibonacci);
		System.out.println("Nr of operations: " + nrOfOperations);
	}

	static void printNumbers() {
		for (int i = 0; i <= 10; i++) {
			System.out.print(i + " ");
		}
	}

	static void printNumbersRecursively(int number) {
		System.out.println(number++);
		if (number >= 10) {
			System.out.println(number);
			return;
		} else {
			printNumbersRecursively(number);
		}
	}

	static int fiboRecursively(int number) {
		nrOfOperations++;
		System.out.println(number);
		if (number <= 1) {
			return number;
		}
		int value = fiboRecursively(number - 2) + fiboRecursively(number - 1);
		System.out.println(value);
		return value;

	}

	static void fibo() {
		int next = 0;
		int first = 0;
		int second = 1;
		int n = 13;
		System.out.print(first + " ,");
		System.out.print(second + " ,");
		for (int i = 1; i < n; i++) {
			next = first + second;
			first = second;
			second = next;
			System.out.print(next + " ,");
		}
	}

}
