package com.vincze.sda.fundamentals.main.datastructures;

public class List {
	private int[] values = new int[10];

	/**
	 * This method is used to add elements in list
	 * 
	 * @param value - to add inside array
	 * @param pos   - index, where the value is added
	 */

	public void addElement(int value, int pos) {
		checkSize(pos);
		values[pos] = value;
	}
	
	public int getElement(int pos) {
		return values[pos];
	}
	
	public int size() {
		return values.length;
	}
	public void checkSize(int pos) {
		if (pos > values.length-1) {
			int[] valuesDoubleSize = new int [values.length*2];
			for (int i = 0; i < values.length; i++) {
			valuesDoubleSize[i] = values[i];	
			}
			values = valuesDoubleSize;
		}
	}
}