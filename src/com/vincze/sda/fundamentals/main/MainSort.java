package com.vincze.sda.fundamentals.main;

import java.util.Arrays;
import java.util.Scanner;

public class MainSort {
	static final int ARRAY_SIZE = 5;

	public static void main(String[] args) {
		int[] numbersFromInput = readNumbers();
		int[] copyNumbersFromInput = Arrays.copyOf(numbersFromInput, numbersFromInput.length);
//		int[] orderedNumbers = sortArrayUsingFor(numbersFromInput);
//		int[] orderedNumbers = sortArrayUsingSelection(numbersFromInput);
		printArray(copyNumbersFromInput);
		System.out.println();
//		printArray(orderedNumbers);

		int position = searchNumberPositionInArray(copyNumbersFromInput, 5);
		if (position != -1) {
			System.out.println("Found at: " + position);
		} else {
			System.out.println(5 + " Not foun in array.");
		}
	}

	static int searchNumberPositionInArray(int[] array, int numberToSearch) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == numberToSearch) {
				return i;
			}
		}
		return -1;
	}

	static int[] readNumbers() {
		int[] inputArray = new int[ARRAY_SIZE];
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < ARRAY_SIZE; i++) {
			System.out.println("Enter number " + i + ": ");
			int number = scanner.nextInt();
			inputArray[i] = number;
		}
		return inputArray;
	}

	static int[] sortArrayUsingWhile(int[] numbers) {
		int currentPosition = 0;
		while (currentPosition < numbers.length) {
			for (int i = 0; i < numbers.length - currentPosition - 1; i++) {
				if (numbers[i] > numbers[i + 1]) {
					int temp = numbers[i];
					numbers[i] = numbers[i + 1];
					numbers[i + 1] = temp;
				}
			}
			currentPosition++;
		}
		return numbers;
	}

	static int[] sortArrayUsingFor(int[] numbers) {
		for (int j = 0; j < numbers.length; j++) {
			for (int i = 0; i < numbers.length - j - 1; i++) {
				if (numbers[i] > numbers[i + 1]) {
					int temp = numbers[i];
					numbers[i] = numbers[i + 1];
					numbers[i + 1] = temp;
				}
			}
		}
		return numbers;
	}

	static int[] sortArrayUsingSelection(int[] numbers) {

		for (int j = 0; j < numbers.length - 1; j++) {
			int minPosition = j;
			for (int i = j + 1; i < numbers.length; i++) {
				if (numbers[i] < numbers[minPosition]) {
					minPosition = i;
				}
			}
			// System.out.println("Min: " + numbers[minPosition]);
			int temp = numbers[j];
			numbers[j] = numbers[minPosition];
			numbers[minPosition] = temp;
		}
		return numbers;
	}

	static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}
}
